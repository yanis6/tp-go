package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

type MyResponseWriter struct {
	http.ResponseWriter
	code int
}

func (mw *MyResponseWriter) WriteHeader(code int) {
	mw.code = code
	mw.ResponseWriter.WriteHeader(code)
}

func RunSomeCode(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Got a %s request for: %v", r.Method, r.URL)
		myrw := &MyResponseWriter{ResponseWriter: w, code: -1}
		handler.ServeHTTP(myrw, r)
		log.Println("Response status: ", myrw.code)
	})
}

func timeHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {

	case http.MethodGet:

		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		time := time.Now()
		heure := time.Hour()
		minutes := time.Minute()

		fmt.Fprintf(w, "%02dh%02d", heure, minutes)
		w.WriteHeader(http.StatusOK)
	}
}

func helloHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {

	case http.MethodPost:

		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		for key, value := range req.PostForm {
			fmt.Println(key, "=>", value)
		}

		var author = req.PostForm["author"][0]
		var entry = req.PostForm["entry"][0]

		fmt.Println(author, ":", entry)
		fmt.Fprintf(w, "%v : %v\n", author, entry)

		saveFile, err := os.OpenFile("./save.data", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
		defer saveFile.Close()

		write := bufio.NewWriter(saveFile)

		if err == nil {
			fmt.Fprintf(write, "%s\n", entry)
		}

		write.Flush()
		w.WriteHeader(http.StatusOK)
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request) {

	switch req.Method {

	case http.MethodGet:

		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		file, err := os.Open("save.data")

		if err != nil {
			log.Fatalf("failed to open")
		}

		scanner := bufio.NewScanner(file)

		scanner.Split(bufio.ScanLines)
		var text []string

		for scanner.Scan() {
			text = append(text, scanner.Text())
		}

		file.Close()

		for _, each_ln := range text {
			fmt.Println(each_ln)
			fmt.Fprintf(w, "%v\n", each_ln)
		}

		w.WriteHeader(http.StatusOK)
	}
}

func main() {

	fmt.Println("==========\n START TP \n==========")

	mux := http.NewServeMux()

	mux.HandleFunc("/", timeHandler)
	mux.HandleFunc("/hello", helloHandler)
	mux.HandleFunc("/entries", entriesHandler)

	WrappedMux := RunSomeCode(mux)
	log.Fatal(http.ListenAndServe(":4567", WrappedMux))
}
